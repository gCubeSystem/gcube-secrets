This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for GCube Secrets

## [v1.1.0-SNAPSHOT]

- The function getRoles() now returns all owner roles [#28836]
- Added getContextRoles() [#28836]


## [v1.0.0] 

- First Release

