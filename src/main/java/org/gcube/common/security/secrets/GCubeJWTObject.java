package org.gcube.common.security.secrets;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GCubeJWTObject {
	
	protected final static List<String> MINIMAL_ROLES = Arrays.asList("Member");
	
	@JsonProperty("aud")
	private String context;

	@JsonProperty("resource_access")
	private Map<String, Roles> contextAccess = new HashMap<>();

	@JsonProperty("realm_access")
	private Roles realmAccess;

	@JsonProperty("preferred_username")
	private String username;

	@JsonProperty("given_name")
	private String firstName;

	@JsonProperty("family_name")
	private String lastName;

	private String clientId;
	
	//"name", as the client pretty name, e.g. Catalogue for gcat, Workspace for storage-hub
	@JsonProperty("name")
	private String clientName; 
	
	//username of the user who requested such client
	@JsonProperty("contact_person")
	private String contactPerson; 
	
	//the name of the organisation / community using such client. D4Science will be used for internal clients.
	@JsonProperty("contact_organisation")
	private String contactOrganisation;		

	private static final String INTERNAL_CLIENT_ORGANISATION_NAME =  "D4Science";

	@JsonProperty("email")
	private String email;
	
	@JsonProperty("exp")
	private long expirationTime;
	
	
	public List<String> getRoles(){
		return contextAccess.get(this.context) == null ? MINIMAL_ROLES : contextAccess.get(this.context).roles;
	}

	public List<String> getRealmRoles(){
		return realmAccess == null? Collections.emptyList() : realmAccess.roles ;
	}

	public String getContext() {
		try {
			return  URLDecoder.decode(context, StandardCharsets.UTF_8.toString());
		}catch (UnsupportedEncodingException e) {
			return context;
		}
	}

	public String getUsername() {
		return username;
	}

	public boolean isExternalService() {
		return contactOrganisation != null && contactOrganisation.equals(INTERNAL_CLIENT_ORGANISATION_NAME);
	}

	public boolean isApplication() {
		return clientId != null;
	}

	@JsonSetter("client_id")
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	/**
	 * Maintianing the backward compatibility with the old name
	 * @param clientId
	 */
    @JsonSetter("clientId")
    private void setMediaAsset(String clientId) {
        if (this.clientId == null) {
            setClientId(clientId);
        }
    }

	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public long getExpirationTime() {
		return expirationTime;
	}
	
	@Override
	public String toString() {
		return "GcubeJwt [context=" + getContext() + ", roles=" + getRoles() + ", username=" + username
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
	}
	
	public String getClientName() {
		return clientName;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public String getContactOrganisation() {
		return contactOrganisation;
	}
	
	public static class Roles {

		@JsonProperty("roles")
		List<String> roles = new ArrayList<>();

	}

}

