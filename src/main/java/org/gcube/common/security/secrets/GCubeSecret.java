package org.gcube.common.security.secrets;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import org.gcube.common.authorization.client.Constants;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.ClientType;
import org.gcube.common.security.Owner;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GCubeSecret extends Secret {

	public static final String GCUBE_TOKEN_REGEX = "([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})(-[0-9]+)?";

	private String gcubeToken;
	private Owner owner;
	private String context;

	public GCubeSecret(String gcubeToken) {
		this.gcubeToken = gcubeToken;
	}

	private void init() throws Exception{
		AuthorizationEntry authorizationEntry = Constants.authorizationService().get(gcubeToken);
		this.owner = new Owner(authorizationEntry.getClientInfo().getId(), 
				authorizationEntry.getClientInfo().getRoles(), authorizationEntry.getClientInfo().getType()==ClientType.EXTERNALSERVICE,
				authorizationEntry.getClientInfo().getType()==ClientType.SERVICE);
		this.context = authorizationEntry.getContext();
	}

	
	

	@Override
	public Owner getOwner() {
		if (Objects.isNull(owner)) 
			try {
				init();
			} catch (Exception e) {
				throw new RuntimeException("error retrieving context",e);
			}

		return owner;
	}

	@Override
	public String getContext()  {
		if (Objects.isNull(context))
			try {
				init();
			} catch (Exception e) {
				throw new RuntimeException("error retrieving context",e);
			}

		return context;
	}

	@Override
	public Map<String, String> getHTTPAuthorizationHeaders() {
		Map<String, String> authorizationHeaders = new HashMap<>();
		authorizationHeaders.put(org.gcube.common.authorization.client.Constants.TOKEN_HEADER_ENTRY, gcubeToken);
		return authorizationHeaders;
	}

	@Override
	public boolean isValid() {
		return gcubeToken != null && !gcubeToken.isEmpty() && Pattern.matches(GCUBE_TOKEN_REGEX, gcubeToken);		
	}

	@Override
	public boolean isExpired() {
		return false;
	}

}
