package org.gcube.common.security.secrets;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.keycloak.KeycloakClient;
import org.gcube.common.keycloak.KeycloakClientFactory;
import org.gcube.common.keycloak.model.AccessToken;
import org.gcube.common.keycloak.model.ModelUtils;
import org.gcube.common.keycloak.model.PublishedRealmRepresentation;
import org.gcube.common.security.Owner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UmaTokenSecret extends Secret {

	private static final Logger log = LoggerFactory.getLogger(UmaTokenSecret.class);

	private static final String AUTH_HEADER = "Authorization";
	private static final String USER_HEADER = "d4s-user";

	private String encodedUmaToken;

	private Owner owner;
	private String context;

	private AccessToken accessToken;

	private boolean initialised = false;

	public UmaTokenSecret(String encodedUmaToken) {
		this.encodedUmaToken = encodedUmaToken;
	}

	@Override
	public Owner getOwner() {
		init();
		return this.owner;
	}

	@Override
	public String getContext() {
		init();
		return this.context;
	}

	@Override
	public Map<String, String> getHTTPAuthorizationHeaders() {
		Map<String, String> authorizationHeaders = new HashMap<>();
		authorizationHeaders.put(AUTH_HEADER, "Bearer " + this.encodedUmaToken);
		String encodedUser = Base64.getEncoder().encodeToString(this.getOwner().getId().getBytes());
		authorizationHeaders.put(USER_HEADER, encodedUser);
		return authorizationHeaders;

	}

	protected String getEncodedUmaToken() {
		return encodedUmaToken;
	}

	public boolean isExpired() {
		init();
		return accessToken.isExpired();
	}

	private synchronized void init() {
		if (!initialised)
			try {

				String realAccessTokenEncoded = encodedUmaToken.split("\\.")[1];

				String decodedAccessPart = new String(Base64.getDecoder().decode(realAccessTokenEncoded.getBytes()));

				ObjectMapper objectMapper = new ObjectMapper();

				this.accessToken = objectMapper.readValue(decodedAccessPart, AccessToken.class);
				GCubeJWTObject obj = objectMapper.readValue(decodedAccessPart, GCubeJWTObject.class);
				owner = new Owner(obj.getUsername(), obj.getRoles(), obj.getRealmRoles(), obj.getEmail(),
						obj.getFirstName(), obj.getLastName(), obj.isExternalService(), obj.isApplication());
				owner.setClientName(obj.getClientName());
				owner.setContactOrganisation(obj.getContactOrganisation());
				owner.setClientName(obj.getClientName());
				context = obj.getContext();

				initialised = true;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

	}

	@Override
	public boolean isValid() {
		init();
		try {
			KeycloakClient client = KeycloakClientFactory.newInstance();
			PublishedRealmRepresentation realmInfo = client.getRealmInfo(client.getRealmBaseURL(context));
			return ModelUtils.isValid(encodedUmaToken, realmInfo.getPublicKey(), false);
		} catch (Exception e) {
			log.error("Error contacting keycloak, is not possible to check token validity", e);
			return false;
		}
	}
	


}
