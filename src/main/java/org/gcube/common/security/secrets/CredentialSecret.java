package org.gcube.common.security.secrets;

import java.util.Map;

import org.gcube.common.keycloak.KeycloakClient;
import org.gcube.common.keycloak.KeycloakClientFactory;
import org.gcube.common.keycloak.model.TokenResponse;
import org.gcube.common.security.Owner;

public class CredentialSecret extends Secret {

	protected boolean initialised = false;

	private String username;
	private String password; 
	private String context;
	private UmaTokenSecret accessTokenSecret;


	public CredentialSecret(String username, String password, String context) {
		this.username = username;
		this.password = password;
		this.context = context;
		init();
	}

	private void init() {
		refreshAccessToken();
	}

	private void refreshAccessToken() {
		try {
			KeycloakClient client = KeycloakClientFactory.newInstance();
			TokenResponse response = client.queryUMAToken(context, username, password, context, null);
			this.accessTokenSecret = new UmaTokenSecret(response.getAccessToken());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Owner getOwner() {
		return this.accessTokenSecret.getOwner();
	}

	@Override
	public String getContext() {
		if (this.accessTokenSecret.isExpired())
			refreshAccessToken();
		return this.accessTokenSecret.getContext();
	}

	@Override
	public Map<String, String> getHTTPAuthorizationHeaders() {
		if (this.accessTokenSecret.isExpired())
			refreshAccessToken();
		return this.accessTokenSecret.getHTTPAuthorizationHeaders();
	}

	@Override
	public boolean isExpired() {
		return this.accessTokenSecret.isExpired();
	}
	
	@Override
	public boolean isValid() {
		return this.accessTokenSecret.isValid();
	}
}
